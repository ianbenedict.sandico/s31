//Model -> routes -> controllers ->index.js

//Route => contains all the endpoint for our application
//We seperate the routes such that "index.js" only contains information on the server
//We need to use express Router() to achieve this
const express = require('express');
//Creates a router instance that functions as a middleware and routing system
//Allows access to HTTP middlewares that makes it easier to create routes for our application
const router = express.Router();
//The 
const taskController = require('../controllers/taskController');

//Routes are responsible for the URIs that our client accesses and the corresponding controller functions that will be used when a route is accessed.
//localhost:3001/tasks
//localhost:3001/tasks/getAll
//Task route extension, extend base
router.get('/getAll', (req,res) =>{
	//Invokes the "getAllTasks" function from the "taskcontroller.js" file and sends the result back to the client
	
	//"resultFromController" is only used here to make the code easier to understand but it's common practice to use the shorthand paramater name for a result using the parameter name "result"/"res"
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Create new task
//we use post, so we need req.body
router.post('/',(req,res) =>{
	//the createTask function needs the data from the request body, so we need to supply it to the function
	//If information will be coming from the client side (post) commonly from form, the data can be accessed from the request "body" property
	taskController.createTask(req.body).then(result => res.send(result));
})

//to delete a task
//This route expects to receive a DELETE request at the URL "/task/id"
//http://localhost:3001/tasks/:id
//the task ID is obtained from the URL is denoted by the ":id" identifier in the route
//The colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
//The word that comes after the(:) symol will be the name of the URL parameter
//Wildcard
//"id" is a WILDCARD where you can put any value, it then creates link between "id" parameter in the URL and then value provided in the URL
//Ex	
	//if the route si localhost:3000/tasks/1234
	//1234 is assigned to the "id" parameter in the URL
//if data needed from client is from body ---> req.body || property URL -->> req.params 
router.delete('/:id', (req,res) =>{
	//If the information will be coming from the URL, the data can be accessed from the request "params" property
	//In this case "id" is the name of the parameter, the property name of this object will match the given URL parameter
	//req.params.id
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})


//1.get specific task
router.get('/:id',(req,res) =>{
	taskController.getOneTask(req.params.id).then(result =>res.send(result));
})

//update status to "complete"
router.put('/:id/complete', (req,res) =>{
	taskController.updateTask(req.params.id, req.body).then(result =>res.send(result));
})

//Used so the files can be resusable
module.exports = router;
