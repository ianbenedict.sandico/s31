//Model -> routes -> controllers ->index.js

//Contollers contain the functions and business logic of our Express js applications
//Meaning all the operations it can do will be places in this file

//Allows us to use the contents of the "task.js" file in the models folder
const Task = require('../models/task');

//Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	
	//The "return" statement, returns the result of the mongoose method "find" back to "taskRoute.js" file which invokes this function when the "/tasks" routes is accssed
	//The "then" method is used to wait for the Mongoose "find" method to finish before sending the result back and eventually to the client
	return Task.find({}).then(result =>{
		//The "return" here, returns the result of the MongoDB query to the "result" parameter defined in the "then" method
		return result;
	})
}




//The request body coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed "requestBody"
module.exports.createTask = (requestBody) => {
	//Create a task object based on the mongoose model Task
	let newTask = new Task ({
		name: requestBody.name
	})
	//Cannot use .then, if there are no return statement
	//saves the newly created "newTask" object in the MongoDB database
	//.then method waits until the taks is stored/error
	//.then method will accept 2 arguments:
			//First Parameter will store the result returned by the save method
			//Second parameter will store the roor object
	//Compared to using a callback function Mongoose methods discussed in the previous session, the first parameter strores the result and the error is stored in the second parameter.
	return newTask.save().then((task, error) => {
		//if an error is encountered, the "return" statement will prevent any other line or code below it and within the same code block from executing
		//Since the following return statement is nested within the ".then" method chained to the "save" method, they do not prevent each other from executing code.
		if(error){
			console.log(error);
			return false;
		}else{
			return task;
		}
	})
}

//Delete task
//Bussiness Logic
/*
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{
		if(err){
			//if an error is encountered returns a "false" boolean back to the client	
			console.log(err)
			return false;
		}else{
			//Delete successful, returns the removed task object back to the client 
			return removedTask;
		}
	})
}

//Updating a task
//Business Logic
/*
1. Get the task with the id using the method "findById"
2.Replace the task's name returned from the database with the "name" property from the request body
*/

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(err)
			return false;
		}

		//results of the "findById" method will be stored in the "result" parameter
		//It's "name" property will be reassigned the value of the "name" received from body
		result.name = newContent.name;
		return result.save().then((updatedTask,saveErr) =>{
			if(saveErr){
				console.log(saveErr)
				return false;
			}else{
				//update successfully, returns the updated task object back to the client
				return updatedTask;
			}
		})
	})
}

// retrieve tasks
module.exports.getOneTask = (taskId) => {
	return Task.findById(taskId).then((result,error) =>{
		if(error){
			console.log(error)
			return false;
		}
			return result;
	})
}

//update status
module.exports.updateTask =(taskId, newContent) => {
	return Task.findById(taskId).then((result,error) =>{
		if(error){
			console.log(err)
			return false;
		}
		result.status = newContent.status;
		return result.save().then((updatedTask, saveErr) =>{
			if(saveErr){
				console.log(saveErr)
				return false;
			}else{
				return updatedTask;
			}
		})
	})
}