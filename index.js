//Model -> routes -> controllers ->index.js

//Setup the dependencies
const express = require('express');
const mongoose = require('mongoose');
//This allows us to use all the routes defined in taskRoutes.js
const taskRoute = require('./routes/taskRoute');                //2nd


//Server setup
const app = express();
const port = 3002;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Database connection
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.o2ycp.mongodb.net/batch127_to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
);


//connection error handling

let db= mongoose.connection;
db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database"))


//Add the task routes 											//2nd
//Allows the task routes created in "taskRoutes.js" file to use "/tasks" route
//Base url, check extension url @ taskRoute
app.use('/tasks', taskRoute);
//localhost:3001/tasks

app.listen(port,() => console.log(`Now listening to ${port}`))

/*
task.js(model)---> taskcontroller-->taskRoutes-->index.js
*/

// Modules-Self-contained units of functionality that can be shared and reused

// module.exports.getAll									- to connect routes, or other documents

// const router = express router = express.Router()  		 -  to share routes and endpoints for reuse
// module.exports= router

// module.exports = mongoose.model('course', courseSchema) - to export model

// const Course = require('../models/course')
// return Course.find({ isActive:true}).then(course =>)

// Seperation of concerns

// ROUTES- 
// Endpoints, when particular controller will be used
// specific controller action will be called when a specific HTTP method is received on a specific API endpoint
// router.get()



// CONTROLLERS-

// Contains how API will perform, task.findOne, Model.find(), Model.findbyIdAndUpdate(), findbyIdAndDelete,model
// Ex.
// module.exports.getAll=() =>{
// 	return
// }


// MODELS- 
// Contains What objects are needed in our API(Users, courses), Schemas, relationships
// 2 data designs- referenced and embedded
// Ex.

// Const courseSchema= newmongoose.Schema({
// 	name: {
// 	},
// 	description: {
// 	},
// 	price:{
	
// 	}
// })
// */

// //Model -> Controllers -> Routes -> index.js(main server)